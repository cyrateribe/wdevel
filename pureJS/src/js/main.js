const getHotModelsUrl = "http://stock.ssangyong.pl/api/getHotModels/",
    getHotOffersUrl = "http://stock.ssangyong.pl/api/getHotoffers/",
    offerImageUrl = "http://www.ssangyong.pl/konfigurator-images/images/";

let models = [], selectedModels = [], sortOptions = "asc", offers = [];

(function () {

    initModelsList();

    initCarsList();

    document.getElementById('sort-button').addEventListener('click', updateSort)
})();

//document.querySelectorAll('input[name=model]:checked');

function initModelsList() {
    let modelsReq = new XMLHttpRequest();
    modelsReq.onload = modelsListener;
    modelsReq.onerror = modelsError;
    modelsReq.open('get', getHotModelsUrl, true);
    modelsReq.send();
}

function modelsListener() {
    let response = JSON.parse(this.responseText);
    let modelsDOM = document.getElementById('models-checkboxes');
    if (Array.isArray(response) && response.length > 0) {
        models = response;
        models.map((model) => {
            modelsDOM.insertAdjacentHTML('beforeend', `<div>
                    <label for="car-model_${model.model}">
                    <input class="model-check" type="checkbox" name="model" id="car-model_${model.model}" value="${model.model}">
                    <span></span>
                    ${model.model}</label>
                    </div>`);
        });

        let modelsCheckboxes = document.querySelectorAll('input.model-check');

        Array.from(modelsCheckboxes).map((checkbox) => {
            checkbox.addEventListener("change", updateSelectedModels)
        });

    } else {
        modelsDOM.innerHTML = `<h4>Brak modeli</h4>`;
    }
}

function modelsError(err) {
    console.log('Fetch Error :-S', err);
}

function updateSelectedModels(event) {
    let defaultElem = event.target;
    if (defaultElem.checked === true) {
        selectedModels.push(defaultElem.defaultValue);
    } else {
        let index = selectedModels.indexOf(defaultElem.defaultValue);
        selectedModels.splice(index, 1);
    }
    initCarsList();
}

function updateSort(event) {
    // console.log(event.srcElement.getAttribute('data-sort'));
    let sortElem = event.target;
    if (sortElem.getAttribute('data-sort') == 'asc') {
        sortOptions = 'desc';
    }
    else {
        sortOptions = 'asc';
    }
    sortElem.setAttribute('data-sort', sortOptions);
    initCarsList();

}

function initCarsList() {
    let offersReq = new XMLHttpRequest();
    offersReq.open("POST", `${getHotOffersUrl}`, true);
    offersReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    offersReq.onreadystatechange = () => {
        if (offersReq.readyState == XMLHttpRequest.DONE && offersReq.status == 200) {
            offers = JSON.parse(offersReq.responseText);
            if (Array.isArray(offers) && offers.length > 0) {
                let offersDom = document.getElementById('offers-list');
                offersDom.innerHTML = "";
                offers.map((offer) => {
                    // console.log(offer);
                    offersDom.insertAdjacentHTML('beforeend', `<article class="status-${offer.status}">
                    <div class="offer-image-wrapper status-${offer.status}">
                        <img src="${offerImageUrl + offer.params.model.toLowerCase()}/${offer.params.year}/colors/cars/${offer.params.color.replace(/\s+/g, '_').toLowerCase()}.png" alt="${offer.title}">
                        <div class="car-model">
                            <h5><strong>${offer.params.model}</strong> ${offer.params.trim}</h5>
                            <p class="new-used">${offer.new_used == "new" ? "Nowy" : "Używany"}</p>
                        </div>
                    </div>
                    
                    <div class="offer-main-info">
                        <h6><strong>${offer.params.engine_capacity} ${offer.params.fuel_type}</strong>
                            ${offer.params.transmission} ${offer.params.gearbox} <span class="separation"></span> ${offer.params.color}</h6>
                        <div class="info-table">
                            <div class="row">
                                <div class="left"><p>Wersja</p></div>
                                <div class="right"><p>${offer.params.trim}</p></div>
                            </div>
                              <div class="row">
                                <div class="left"><p>Rok produkcji</p></div>
                                <div class="right"><p>${offer.params.year}</p></div>
                            </div>
                              <div class="row">
                                <div class="left"><p>Rok modelowy</p></div>
                                <div class="right"><p>${offer.params.my}</p></div>
                            </div>
                              <div class="row">
                                <div class="left"><p>Wyposażenie</p></div>
                                <div class="right"><p>${offer.params.option}</p></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="price-wrapper">
                        <div class="base-price">
                            <div class="price-name">Cena bazowa: </div>
                            <p>${offer.params.price.srp} <span class="price-currency">${offer.params.price.currency}</span></p>
                        </div>
                         <div class="discount-price">
                            <div class="price-name">Rabat: </div>
                            <p>-${offer.params.price.discount} <span class="price-currency">${offer.params.price.currency}</span></p>
                        </div>
                         <div class="hot-price">
                            <div class="price-name">Gorąca cena: </div>
                            <p>${offer.params.price.hot_price} <span class="price-currency">${offer.params.price.currency}</span></p>
                        </div>
                        <div class="ask-wrapper">
                            <a href="mailto:${offer.dealer_email}">Zapytaj</a>
                        </div>
                    </div>
                    </article>`);
                });
            }
        }
    }
    offersReq.send(`model=${selectedModels.toString()}&sort[hot_price]=${sortOptions}`);
}

